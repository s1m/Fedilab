Il prend en charge :
- Mastodon, Pleroma, Pixelfed et Friendica.

L’application possède des fonctionnalités avancées :

- Composition de fils
- Support multicomptes
- Planification de messages depuis l'application
- Abonnement et interaction avec des instances distantes
- Actions entre comptes grâce aux appuis longs
- Option de traduction
- Fils artistiques
